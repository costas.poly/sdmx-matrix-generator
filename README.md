# sdmx-matrix-generator
This tool is used to visually model and design SDMX data artefacts, and generate the SDMX-ML markup for implementation. It is an enhancement of the Generic SDMX Design Matrix which has proven to be successful as a collaborative design tool for non-SDMX experts. 

## Macro security issues, Excel crashes, and how to overcome them
Users may have problems enabling the macros when first opening the SDMX Matrix Generator. Here is how to enable them. However, this will only work if your agency has not  completely blocked macro-enabled workbooks:
1. Downloaded the SDMX Matrix Generator and open it
2. Click **Enable macros** at the top
3. Save the SDMX Matrix Generator to a trusted location, usually on the C: drive or check the MS Office trusted locations
4. Close the SDMX Matrix Generator
5. Open the SDMX Matrix Generator from the trusted location

In case the saved file refuses to open, for example it immediately disappears, do the following:
1. Open the Excel application (not the SDMX Matrix Generator)
2. Go to **File/Open/Browse** to display the **Open** dialog
3. Navigate to the SDMX Matrix Generator in the trusted file location
4. Next to the **Open** button there is a small arrow. Click it and selection **Open and Repair...**. The SDMX Matrix Generator should now open correctly. There may be an info dialog that you can clear
5. Save and close the SDMX Matrix Generator
6. Open the SDMX Matrix Generator from the trusted location
7. Try** Generating SDMX Artefacts**. You may have to change the **Output folder** and Log file(full path) to be valid for you

## Accompanying .Stat Academy course
[An Introduction to SDMX Structural Modelling for Data Producers](https://academy.siscc.org/courses/an-introduction-to-sdmx-structural-modelling-for-data-producers/) to learn how to model data with the SDMX Matrix Generator.

The primary goal of the tool is to be able to create the artefacts without a lot of SDMX technical knowledge, and to put the focus on the statistical aspects of the data model. 

This tool maps to these GSBPM sub-processes:
  - 2.1 Design outputs (Dataflows design in Dataflows and DSD-Concept Matrix worksheets)
  - 2.2 Design variable descriptions (decomposition and creation of indicators in Indicator-Concept Matrix worksheet)
  - 3.1 Reuse or build collection instruments (generating SDMX-ML for reporting Dataflows)
  - 3.3 Reuse or build dissemination instruments (generating SDMX-ML for dissemination Dataflows)

To improve the tool, please request it by raising an issue and/or fork the repository and make the improvement yourself :)

Step-by-step guide: See the link above to the full User Guide powerpoint.

[David Barraclough](https://gitlab.com/dbarraclough99)
